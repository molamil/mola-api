# Mola::Api

Molamil API gems with shared common JSON API responses.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'mola-api', '~> 1.1.3', git: 'https://bitbucket.org/molamil/mola-api.git'
```

And then execute:

```
$ bundle install
```

## Usage

TODO: Write usage instructions here

## Development

After checking out the repo, run `bin/setup` to install dependencies. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Releasing a new version

- Bump version number in lib/mola/api/version.rb
- Commit changes & push changes.
- git tag -a v<version_number>
- git push origin master
- git push --tags origin master

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request

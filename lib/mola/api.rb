require 'rails'
require 'json'

require 'mola/api/version'
require 'mola/api/json'
require 'mola/api/rescue_exceptions_with_json'

module Mola
  module Api

    def self.root
      File.expand_path('../..', __FILE__)
    end

  end
end

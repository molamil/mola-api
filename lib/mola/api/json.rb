module Mola
  module Api
    class Json

      def initialize(controller)
        @controller = controller
      end

      ##########
      # Success:
      ##########

      # 200: Ok
      def ok(data: nil, meta: nil, status: :ok)
        render_data_json(data: data, meta: meta, status: status)
      end

      # 201: Created
      def created(data: nil)
        render_data_json(data: data, status: :created)
      end

      # 202: Accepted
      def accepted
        render_empty_nothing(status: :accepted)
      end

      # 204: No content
      def no_content
        render_empty_nothing(status: :no_content)
      end

      #############
      # Redirection
      #############

      # 301: Moved permanently
      def moved_permanently(location: nil)
        render_redirect_json(location: location, status: :moved_permanently)
      end

      # 302: Found
      def found(location: nil)
        render_redirect_json(location: location, status: :found)
      end

      # 303: See other
      def see_other(location: nil)
        render_redirect_json(location: location, status: :see_other)
      end

      ##############
      # Client error
      ##############

      # 400: Bad request
      def bad_request(message: "Bad request", code: "bad_request")
        render_error_json(message: message, code: code, status: :bad_request)
      end

      # 401: Unauthorized
      def unauthorized(message: "Unauthorized", code: "unauthorized")
        render_error_json(message: message, code: code, status: :unauthorized)
      end

      # 403: Forbidden
      def forbidden(message: "Forbidden", code: "forbidden")
        render_error_json(message: message, code: code, status: :forbidden)
      end

      # 404: Not found
      def not_found(message: "Not found", code: "not_found")
        render_error_json(message: message, code: code, status: :not_found)
      end

      # 409: Conflict
      def conflict(message: "Conflict", code: "conflict")
        render_error_json(message: message, code: code, status: :conflict)
      end

      # 422: Unprocessable entity
      def unprocessable_entity(message: "Unprocessable entity", code: "unprocessable_entity", errors: [])
        render_error_json(message: message, code: code, status: :unprocessable_entity, errors: errors)
      end

      ##############
      # Server error
      ##############

      # 500: Internal server error
      def internal_server_error(message: "Internal server error", code: "internal_server_error")
        render_error_json(message: message, code: code, status: :internal_server_error)
      end

      private

        def render_data_json(data: nil, meta: nil, status: :ok)
          if data
            result = {}

            result[:status] = 'success'
            result[:meta] = meta if meta
            result[:data] = data

            render_as_json(data: result, status: status)
          elsif status === :created
            @controller.head(:created)
          else
            @controller.head(:no_content)
          end
        end

        def render_empty_nothing(status: :ok)
          @controller.head(status)
        end

        def render_redirect_json(location: nil, status: :moved_permanently)
          result = {}

          result[:status] = 'redirect'
          result[:location] = location

          render_as_json(data: result, status: status)
        end

        def render_error_json(message: nil, code: nil, status: :bad_request, errors: [])
          result = {}

          result[:status] = 'error'
          result[:message] = message
          result[:code] = code
          result[:errors] = errors if errors.size > 0

          render_as_json(data: result, status: status)
        end

        def render_as_json(data: nil, status: nil)
          @controller.render(json: data, status: status)
        end

    end
  end
end

module Mola
  module Api
    module RescueExceptionsWithJson
      
      extend ActiveSupport::Concern
      include ActiveSupport::Rescuable
      
      included do
        
        # 'rescue_from' is evaluated bottom-to-top so we rescue general exception last.
        
        rescue_from Exception do |exception|
          Mola::Api::Json.new(self).internal_server_error(message: exception.message)
        end
        
        rescue_from ActionController::ParameterMissing do |exception|
          Mola::Api::Json.new(self).bad_request(message: exception.message)
        end
        
        rescue_from ActiveRecord::RecordNotFound do |exception|
          Mola::Api::Json.new(self).not_found(message: exception.message, code: "not_found")
        end
        
      end
      
    end
  end
end
